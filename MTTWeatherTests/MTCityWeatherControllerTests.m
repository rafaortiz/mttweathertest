//
//  MTCityWeatherControllerTests.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MTCityWeatherController.h"
#import "OHHTTPStubs.h"
#import "OHPathHelpers.h"

@interface MTCityWeatherControllerTests : XCTestCase

@end

@implementation MTCityWeatherControllerTests

- (void)testShouldBeAbleToConvertWeatherCorrectly {
	id<OHHTTPStubsDescriptor> stub =
	[OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
		BOOL containsWeatherURL = [request.URL.absoluteString containsString:@"weather.ashx"];
		XCTAssertTrue(containsWeatherURL);
		XCTAssertEqualObjects(@"GET", request.HTTPMethod);
		XCTAssertNil(request.HTTPBody);
		return containsWeatherURL;
	} withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
		return [OHHTTPStubsResponse responseWithFileAtPath:OHPathForFile(@"weather.json", self.class)
												statusCode:200
												   headers:@{@"Content-Type":@"application/json"}];
	}];
	
	XCTestExpectation *expectation = [self expectationWithDescription:@"Exectation for contact request"];
	
	NSURLSessionTask *task = [MTCityWeatherController getWeatherFromCity:@"Dublin" withBlock:^(NSArray *currentConditions, NSArray *weatherLists, NSError *error) {
		XCTAssertNil(error);
		XCTAssertNotNil(currentConditions);
		XCTAssertNotNil(weatherLists);
		XCTAssertEqual(currentConditions.count, 1);
		XCTAssertEqual(weatherLists.count, 5);
		
		[expectation fulfill];
	}];
	
	[self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
		[OHHTTPStubs removeStub:stub];
	}];
}

@end
