//
//  MTHourlyWeatherTests.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "MTHourlyWeather.h"
#import "MTModelConverter.h"

@interface MTHourlyWeatherTests : XCTestCase

@property (nonatomic, strong) NSDictionary *weatherData;

@end

@implementation MTHourlyWeatherTests

- (void)setUp {
	[super setUp];
	
	self.weatherData =
	@{
	  @"tempC": @"10",
	  @"tempF": @"41",
	  @"time": @"02:30 AM",
	  @"weatherIconUrl": @[@{@"value": @"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"}],
	  @"weatherDesc": @[@{@"value": @"Partly Cloudy"}]
	  };
}

- (void)testShouldBeAbleToParseAWeatherObjectFromJSON {
	MTHourlyWeather *weather = [MTModelConverter convertModelFromJSON:self.weatherData
																 class:[MTHourlyWeather class]];
	
	XCTAssertNotNil(weather);
	[self validateWeather:weather];
}

- (void)testShouldBeAbleToSerializeAWeatherObjectToArchive {
	MTHourlyWeather *weather = [MTModelConverter convertModelFromJSON:self.weatherData
																 class:[MTHourlyWeather class]];
	
	NSData *weatherData = [NSKeyedArchiver archivedDataWithRootObject:weather];
	
	MTHourlyWeather *unarchivedWeather = [NSKeyedUnarchiver unarchiveObjectWithData:weatherData];
	XCTAssertNotNil(unarchivedWeather);
	
	[self validateWeather:unarchivedWeather];
}

- (void)validateWeather:(MTHourlyWeather *)weather {
	
	XCTAssertEqualObjects(weather.tempC, self.weatherData[@"tempC"]);
	XCTAssertEqualObjects(weather.tempF, self.weatherData[@"tempF"]);
	XCTAssertEqualObjects(weather.time, self.weatherData[@"time"]);
	XCTAssertEqualObjects(weather.weatherIconUrl, self.weatherData[@"weatherIconUrl"]);
	XCTAssertEqualObjects(weather.weatherDesc, self.weatherData[@"weatherDesc"]);
}

@end
