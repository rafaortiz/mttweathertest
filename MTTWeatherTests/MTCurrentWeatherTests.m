//
//  MTCurrentWeatherTests.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "MTCurrentWeather.h"
#import "MTModelConverter.h"

@interface MTCurrentWeatherTests : XCTestCase

@property (nonatomic, strong) NSDictionary *weatherData;

@end

@implementation MTCurrentWeatherTests


- (void)setUp {
	[super setUp];
	
	self.weatherData =
	@{
	  @"temp_C": @"10",
	  @"temp_F": @"41",
	  @"observation_time": @"02:30 AM",
	  @"weatherIconUrl": @[@{@"value": @"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"}],
	  @"weatherDesc": @[@{@"value": @"Partly Cloudy"}]
	  };
}

- (void)testShouldBeAbleToParseAWeatherObjectFromJSON {
	MTCurrentWeather *weather = [MTModelConverter convertModelFromJSON:self.weatherData
																 class:[MTCurrentWeather class]];
	
	XCTAssertNotNil(weather);
	[self validateWeather:weather];
}

- (void)testShouldBeAbleToSerializeAWeatherObjectToArchive {
	MTCurrentWeather *weather = [MTModelConverter convertModelFromJSON:self.weatherData
																 class:[MTCurrentWeather class]];
	
	NSData *weatherData = [NSKeyedArchiver archivedDataWithRootObject:weather];
	
	MTCurrentWeather *unarchivedWeather = [NSKeyedUnarchiver unarchiveObjectWithData:weatherData];
	XCTAssertNotNil(unarchivedWeather);
	
	[self validateWeather:unarchivedWeather];
}

- (void)validateWeather:(MTCurrentWeather *)weather {

	XCTAssertEqualObjects(weather.tempC, self.weatherData[@"temp_C"]);
	XCTAssertEqualObjects(weather.tempF, self.weatherData[@"temp_F"]);
	XCTAssertEqualObjects(weather.time, self.weatherData[@"observation_time"]);
	XCTAssertEqualObjects(weather.weatherIconUrl, self.weatherData[@"weatherIconUrl"]);
	XCTAssertEqualObjects(weather.weatherDesc, self.weatherData[@"weatherDesc"]);
}

@end
