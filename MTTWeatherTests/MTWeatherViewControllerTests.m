//
//  MTWeatherViewControllerTests.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OCMock.h"
#import "SVProgressHUD.h"

#import "MTWeatherViewController.h"
#import "MTCityWeatherController.h"
#import "MTModelConverter.h"
#import "MTCurrentWeather.h"

@interface MTWeatherViewControllerTests : XCTestCase

@property (nonatomic, strong) NSArray *currentConditions;
@property (nonatomic, strong) NSArray *weatherList;

@end

@implementation MTWeatherViewControllerTests

- (void)setUp {
	NSArray *weatherData =
	@[@{
		  @"temp_C": @"10",
		  @"temp_F": @"41",
		  @"observation_time": @"02:30 AM",
		  @"weatherIconUrl": @[@{@"value": @"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"}],
		  @"weatherDesc": @[@{@"value": @"Partly Cloudy"}]
		  }];
	
	self.currentConditions = [MTModelConverter convertModelsFromJSON:weatherData
															   class:[MTCurrentWeather class]];
	self.weatherList = [NSArray array];
}

- (void)testShouldLoadCurrentConditionsAndShowCorrectly {
	MTWeatherViewController *viewController = [[MTWeatherViewController alloc] init];
	
	id progressHudMock = OCMClassMock([SVProgressHUD class]);
	OCMExpect([progressHudMock showWithStatus:[OCMArg isEqual:NSLocalizedString(@"Loading weather...", )]]);
	OCMExpect([progressHudMock showSuccessWithStatus:[OCMArg isEqual:NSLocalizedString(@"Success!", )]]);
	
	id cityWeatherControllerMock = OCMClassMock([MTCityWeatherController class]);
	
	OCMExpect([cityWeatherControllerMock getWeatherFromCity:[OCMArg isEqual:@"Dublin"] withBlock:[OCMArg checkWithBlock:^BOOL(MTWeatherCompletionBlock obj) {
		obj(self.currentConditions, self.weatherList, nil);
		return YES;
	}]]);
	
	UIView *view = viewController.view;
	XCTAssertNotNil(view);
	
	NSUInteger numberOfCurrentWeathers = [viewController tableView:nil numberOfRowsInSection:0];
	
	XCTAssertEqual(self.currentConditions.count, numberOfCurrentWeathers);

	
	[cityWeatherControllerMock stopMocking];
	[progressHudMock stopMocking];
}

@end
