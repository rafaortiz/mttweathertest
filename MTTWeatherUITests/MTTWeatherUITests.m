//
//  MTTWeatherUITests.m
//  MTTWeatherUITests
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MTTWeatherUITests : XCTestCase

@end

@implementation MTTWeatherUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void) testAddCity {
	XCUIApplication *app = [[XCUIApplication alloc] init];
	XCUIElement *myCitiesNavigationBar = app.navigationBars[@"My Cities"];
	
	XCUIElementQuery *tablesQuery = app.tables;
	[myCitiesNavigationBar.buttons[@"Add"] tap];
	
	XCUIElement *newYorkStaticText = tablesQuery.staticTexts[@"New York"];
	[newYorkStaticText tap];
}

- (void) testRemoveCity {
	[self testAddCity];
	
	XCUIApplication *app = [[XCUIApplication alloc] init];
	XCUIElement *myCitiesNavigationBar = app.navigationBars[@"My Cities"];
	XCUIElement *editButton = myCitiesNavigationBar.buttons[@"Edit"];
	[editButton tap];
	
	XCUIElementQuery *tablesQuery = app.tables;
	[tablesQuery.buttons[@"Delete New York"] tap];
	[tablesQuery.buttons[@"Delete"] tap];
	[editButton tap];
}

@end
