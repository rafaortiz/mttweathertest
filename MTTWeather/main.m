//
//  main.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
