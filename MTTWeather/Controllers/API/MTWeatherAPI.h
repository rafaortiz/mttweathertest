//
//  MTWeatherAPI.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface MTWeatherAPI : AFHTTPSessionManager

+ (instancetype)sharedAPI;

@end
