//
//  MTWeatherAPI.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTWeatherAPI.h"

NSString *MTWeatherAPIBaseURL = @"http://api.worldweatheronline.com/free/v2/";

@implementation MTWeatherAPI

+ (instancetype)sharedAPI {
	static MTWeatherAPI * _sharedClient = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_sharedClient = [[MTWeatherAPI alloc] initWithBaseURL:[NSURL URLWithString:MTWeatherAPIBaseURL]];
	});
	
	return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
	self = [super initWithBaseURL:url];
	if (self) {
		self.requestSerializer = [AFJSONRequestSerializer serializer];
		self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
		self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/plain", nil];
		NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
																diskCapacity:0
																	diskPath:nil];
		[NSURLCache setSharedURLCache:sharedCache];
	}
	
	return self;
}

@end