//
//  MTCityWeatherController.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTCityWeatherController.h"
#import "MTWeatherAPI.h"
#import "MTModelConverter.h"
#import "MTCurrentWeather.h"
#import "MTWeatherList.h"

@implementation MTCityWeatherController

+ (NSURLSessionTask *)getWeatherFromCity:(NSString *)city withBlock:(MTWeatherCompletionBlock)block {
	if (!city){
		block(nil, nil, [NSError errorWithDomain:NSURLErrorDomain code:-1 userInfo:nil]);
		return nil;
	}
	
	NSDictionary *parameters = @{@"q" : city,
								 @"format" : @"json",
								 @"num_of_days" : @(5),
								 @"key" : MTWeatherAPIKey};
	
	
	return [[MTWeatherAPI sharedAPI] GET:@"weather.ashx"
							   parameters:parameters
								  success:^(NSURLSessionDataTask *task, id responseObject) {
									  
									  NSDictionary* dataDict = responseObject[@"data"];
									  
									  if (dataDict) {

										  NSArray *current = [MTModelConverter convertModelsFromJSON:dataDict[@"current_condition"]
																							   class:[MTCurrentWeather class]];
										  
										  NSArray *weatherList = [MTModelConverter convertModelsFromJSON:dataDict[@"weather"]
																								   class:[MTWeatherList class]];
										  block(current, weatherList, nil);
									  } else {
										  block(nil, nil, [NSError errorWithDomain:NSURLErrorDomain code:-1 userInfo:nil]);
									  }
									  
									  
								  } failure:^(NSURLSessionDataTask *task, NSError *error) {
									  block(nil, nil, error);
								  }];
}

@end
