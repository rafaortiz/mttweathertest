//
//  MTBlocks.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

const static NSString *MTWeatherAPIKey = @"118f777c90e2488c9d6174315161403";

typedef void (^MTCompletionBlock)(NSError *error);

typedef void (^MTObjectCompletionBlock)(id object, NSError *error);

typedef void (^MTArrayCompletionBlock)(NSArray *array, NSError *error);

typedef void (^MTWeatherCompletionBlock)(NSArray *currentConditions, NSArray *weatherLists, NSError *error);
