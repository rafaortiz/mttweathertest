//
//  MTCityWeatherController.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTBlocks.h"

@interface MTCityWeatherController : NSObject

+ (NSURLSessionTask *)getWeatherFromCity:(NSString *)city withBlock:(MTWeatherCompletionBlock)block;

@end
