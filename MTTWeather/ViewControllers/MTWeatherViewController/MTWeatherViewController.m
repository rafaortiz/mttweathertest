//
//  MTWeatherViewController.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 10/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTWeatherViewController.h"
#import "MTCityWeatherController.h"
#import "MTCurrentWeather.h"
#import "MTWeatherCell.h"
#import "MTWeatherList.h"
#import "SVProgressHUD.h"

NSString *MTWeatherCellIdentifier = @"MTWeatherCellIdentifier";

@interface MTWeatherViewController ()

@property (nonatomic, strong) NSArray *currentConditions;
@property (nonatomic, strong) NSArray *weatherLists;

@end

@implementation MTWeatherViewController

- (void) viewDidLoad {
	[super viewDidLoad];
	self.title = self.city.name;
	self.view.backgroundColor = [UIColor whiteColor];
	
	[self configureTableView];
	[self loadWeather];
}

#pragma mark - Private Methods

- (void) configureTableView {
	self.tableView.rowHeight = 61.f;
	self.tableView.allowsSelection = NO;
	self.tableView.accessibilityIdentifier = @"table-weather";

	[self.tableView registerNib:[UINib nibWithNibName:@"MTWeatherCell" bundle:nil]
		 forCellReuseIdentifier:MTWeatherCellIdentifier];
}

- (void) loadWeather {
	[SVProgressHUD showWithStatus:NSLocalizedString(@"Loading weather...", )];
	
	__weak typeof(self) weakSelf = self;
	
	[MTCityWeatherController getWeatherFromCity:self.city.name withBlock:^(NSArray *currentConditions, NSArray *weatherLists, NSError *error) {
		if (error) {
			[SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Ops, we have a problem...", )];
			return;
		}
		
		[SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Success!", )];
		
		weakSelf.currentConditions = currentConditions;
		weakSelf.weatherLists = weatherLists;
		[weakSelf.tableView reloadData];
	}];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.weatherLists.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 1;
	}
	
	NSInteger indexPath = section - 1;
	
	if (self.weatherLists.count > indexPath) {
		MTWeatherList *weatherList = self.weatherLists[indexPath];
		return weatherList.hourly.count;
	}
	
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	MTWeatherCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MTWeatherCellIdentifier
															   forIndexPath:indexPath];
	MTBaseWeather *weather;
	
	if (indexPath.section == 0)  {
		weather = self.currentConditions[indexPath.row];
	} else {
		NSInteger index = indexPath.section - 1;
		
		if (self.weatherLists.count > index) {
			MTWeatherList *weatherList = self.weatherLists[index];
			weather = weatherList.hourly[indexPath.row];
		}
	}
	
	[cell setWeather:weather];
	
	return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 80;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.tableView.frame.size.width - 20, 80)];
	header.backgroundColor = [UIColor whiteColor];
	header.font = [UIFont systemFontOfSize:20];
	header.numberOfLines = 0;
	
	if (section == 0)  {
		header.text = NSLocalizedString(@"Current condition", );
	} else {
		NSInteger index = section - 1;
		
		if (self.weatherLists.count > index) {
			MTWeatherList *weatherList = self.weatherLists[index];
			header.text = [NSString stringWithFormat:@"[%@] %@:%@˚C - %@:%@˚C", weatherList.dateString, NSLocalizedString(@"Min", ), weatherList.mintempC, NSLocalizedString(@"Max", ), weatherList.maxtempC];
		}
	}
	
	return header;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
