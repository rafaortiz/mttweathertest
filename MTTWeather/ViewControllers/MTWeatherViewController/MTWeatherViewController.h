//
//  MTWeatherViewController.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 10/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTBaseViewController.h"
#import "MTCity.h"

@interface MTWeatherViewController : UITableViewController

@property (nonatomic, strong) MTCity *city;

@end
