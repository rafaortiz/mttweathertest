//
//  MTCityListViewController.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTCityListViewController.h"
#import "MTCity.h"
#import "MTDatabase.h"

NSString *CityListCellIdentifier = @"CityListCell";

@interface MTCityListViewController ()

@property (nonatomic, strong) NSArray *cityList;

@end


@implementation MTCityListViewController

#pragma mark - View Lifecycle

- (void) viewDidLoad {
	[super viewDidLoad];
	
	self.title = NSLocalizedString(@"Cities", );
	self.cityList = @[@"Barcelona", @"Dublin", @"London", @"New York"];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.cityList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CityListCellIdentifier];
	
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CityListCellIdentifier];
		cell.separatorInset = UIEdgeInsetsZero;
	}
	
	cell.textLabel.text = self.cityList[indexPath.row];
	
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	RLMResults<MTCity *> *cities = [MTCity objectsWhere:@"name == %@", self.cityList[indexPath.row]];
	
	if (cities.count == 0) {
		MTCity *city = [[MTCity alloc] init];
		city.name = self.cityList[indexPath.row];
		[MTDatabase save:city];
	}
	
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)cancel:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
