//
//  MTMyCitiesViewController.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTMyCitiesViewController.h"
#import "MTWeatherViewController.h"
#import "MTCity.h"
#import "MTDatabase.h"

NSString *MyCitiesCellIdentifier = @"MyCitiesCell";
NSString *ShowWeatherSegue = @"showWeather";

@interface MTMyCitiesViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *editButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *addButton;
@property (nonatomic, strong) RLMResults<MTCity *> *cityList;

@end

@implementation MTMyCitiesViewController

#pragma mark - View Lifecycle

- (void) viewDidLoad {
	[super viewDidLoad];
	
	self.title = NSLocalizedString(@"My Cities", );
	self.editButton.possibleTitles = [NSSet setWithObjects:NSLocalizedString(@"Done", ), NSLocalizedString(@"Edit", ), nil];
	[self loadCities];
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.tableView reloadData];
	self.tableView.hidden = !self.cityList.count;
}

#pragma mark - Private Methods

- (void) loadCities {
	self.cityList = [MTDatabase getAll:[MTCity class]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.cityList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyCitiesCellIdentifier];
	
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyCitiesCellIdentifier];
		cell.separatorInset = UIEdgeInsetsZero;
	}
	
	MTCity *city = self.cityList[indexPath.row];
	cell.textLabel.text = city.name;
	
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	MTCity *city = self.cityList[indexPath.row];
	[self performSegueWithIdentifier:ShowWeatherSegue sender:city];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	// If row is deleted, remove it from the list.
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		
		MTCity *city = self.cityList[indexPath.row];
		[MTDatabase remove:city];
		[self loadCities];
		
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
	}
}

#pragma mark - UIStoryboardSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:ShowWeatherSegue] &&
		[segue.destinationViewController isKindOfClass:[MTWeatherViewController class]] &&
		[sender isKindOfClass:[MTCity class]]) {
		MTWeatherViewController *viewController = (MTWeatherViewController *)segue.destinationViewController;
		viewController.city = (MTCity *)sender;
	}
}

#pragma mark - Actions

- (IBAction)edit:(UIBarButtonItem *)sender {
	[self.tableView setEditing:!self.tableView.editing];
	[self.addButton setEnabled:!self.tableView.editing];
	sender.title = NSLocalizedString((self.tableView.editing) ? @"Done" : @"Edit", );
}

@end
