//
//  MTBaseWeather.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 15/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTBaseWeather.h"

@implementation MTBaseWeather

- (NSString *) icon {
	if (self.weatherIconUrl.count > 0) {
		return self.weatherIconUrl[0][@"value"];
	}
	
	return nil;
}

- (NSString *) desc {
	if (self.weatherDesc.count > 0) {
		return self.weatherDesc[0][@"value"];
	}
	
	return nil;
}

@end
