//
//  MTCity.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <Realm/Realm.h>

@interface MTCity : RLMObject

@property (strong, nonatomic) NSString *name;

@end
