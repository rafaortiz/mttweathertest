//
//  MTCurrentWeather.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 15/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTCurrentWeather.h"

@implementation MTCurrentWeather

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{@"tempC" : @"temp_C",
			 @"tempF" : @"temp_F",
			 @"time" : @"observation_time",
			 @"weatherIconUrl" : @"weatherIconUrl",
			 @"weatherDesc" : @"weatherDesc"};
}

@end

