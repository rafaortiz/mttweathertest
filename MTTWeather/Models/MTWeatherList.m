//
//  MTWeatherList.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTWeatherList.h"
#import "MTHourlyWeather.h"
#import "MTLValueTransformer.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"

@implementation MTWeatherList

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{@"date" : @"date",
			 @"dateString" : @"date",
			 @"maxtempC" : @"maxtempC",
			 @"maxtempF" : @"maxtempF",
			 @"mintempC" : @"mintempF",
			 @"hourly" : @"hourly"};
}

+ (NSValueTransformer *)dateJSONTransformer {
	static NSDateFormatter *formatter = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		formatter = [[NSDateFormatter alloc] init];
		formatter.dateFormat = @"yyyy-MM-dd";
	});
	
	return [MTLValueTransformer transformerUsingForwardBlock:^NSDate *(NSString *value, BOOL *success, NSError **error) {
		return [formatter dateFromString:value];
	} reverseBlock:^NSString *(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
		return [formatter stringFromDate:date];
	}];
}

+ (NSValueTransformer *)hourlyJSONTransformer {
	return [MTLJSONAdapter arrayTransformerWithModelClass:[MTHourlyWeather class]];
}

@end
