//
//  MTWeatherList.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@interface MTWeatherList : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSDate *date;
@property (nonatomic, readonly, copy) NSString *dateString;
@property (nonatomic, readonly, copy) NSString *maxtempC;
@property (nonatomic, readonly, copy) NSString *maxtempF;
@property (nonatomic, readonly, copy) NSString *mintempC;
@property (nonatomic, readonly, copy) NSString *mintempF;
@property (nonatomic, readonly, copy) NSArray *hourly;

@end
