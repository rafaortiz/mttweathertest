//
//  MTModelConverter.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 09/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTModelConverter.h"
#import "MTLJSONAdapter.h"

@implementation MTModelConverter

+ (id)convertModelFromJSON:(NSDictionary *)JSON class:(Class)classToParse {
	NSParameterAssert(classToParse != nil);
	
	@try {
		NSError *error = nil;
		id object = [MTLJSONAdapter modelOfClass:classToParse
							  fromJSONDictionary:JSON
										   error:&error];
		if (!error) {
			return object;
		} else {
			return nil;
		}
	}
	@catch (NSException *exception) {
		return nil;
	}
}

+ (NSArray *)convertModelsFromJSON:(NSArray *)JSON class:(Class)classToParse {
	NSParameterAssert(classToParse != nil);
	
	@try {
		NSError *error = nil;
		NSArray *objects = [MTLJSONAdapter modelsOfClass:classToParse
										   fromJSONArray:JSON
												   error:&error];
		if (!error) {
			return objects;
		} else {
			return nil;
		}
	}
	@catch (NSException *exception) {
		return nil;
	}
}

@end
