//
//  MTBaseWeather.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 15/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@interface MTBaseWeather : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly, copy) NSString *time;
@property (nonatomic, readonly, copy) NSString *tempC;
@property (nonatomic, readonly, copy) NSString *tempF;
@property (nonatomic, readonly, copy) NSArray *weatherIconUrl;
@property (nonatomic, readonly, copy) NSArray *weatherDesc;

@property (nonatomic, readonly) NSString *icon;
@property (nonatomic, readonly) NSString *desc;

@end
