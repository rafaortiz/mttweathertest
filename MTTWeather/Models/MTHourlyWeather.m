//
//  MTHourlyWeather.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTHourlyWeather.h"

@implementation MTHourlyWeather

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{@"tempC" : @"tempC",
			 @"tempF" : @"tempF",
			 @"time" : @"time",
			 @"weatherIconUrl" : @"weatherIconUrl",
			 @"weatherDesc" : @"weatherDesc"};
}

@end
