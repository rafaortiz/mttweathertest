//
//  MTDatabase.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface MTDatabase : NSObject

+ (void) save:(RLMObject *)object;
+ (void) remove:(RLMObject *)object;
+ (id) getAll:(Class)class;

@end
