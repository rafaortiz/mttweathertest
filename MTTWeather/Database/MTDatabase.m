//
//  MTDatabase.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTDatabase.h"

@implementation MTDatabase

+ (void) save:(RLMObject *)object {
	RLMRealm *realm = [RLMRealm defaultRealm];
	[realm transactionWithBlock:^{
		[realm addObject:object];
	}];
}

+ (void) remove:(RLMObject *)object {
	RLMRealm *realm = [RLMRealm defaultRealm];
	[realm beginWriteTransaction];
	[realm deleteObject:object];
	[realm commitWriteTransaction];
}

+ (id) getAll:(Class)class {
	return [class allObjects];
}

@end
