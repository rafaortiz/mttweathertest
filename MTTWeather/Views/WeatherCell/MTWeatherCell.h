//
//  MTWeatherCell.h
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MTBaseWeather;

@interface MTWeatherCell : UITableViewCell

- (void) setWeather:(MTBaseWeather *)currentWeather;

@end
