//
//  MTWeatherCell.m
//  MTTWeather
//
//  Created by Rafael Ortiz on 16/03/16.
//  Copyright © 2016 MTT. All rights reserved.
//

#import "MTWeatherCell.h"
#import "MTBaseWeather.h"
#import "UIImageView+AFNetworking.h"

@interface MTWeatherCell ()

@property (weak, nonatomic) IBOutlet UIImageView *weatherIcon;
@property (weak, nonatomic) IBOutlet UILabel *weatherDesc;
@property (weak, nonatomic) IBOutlet UILabel *temperature;

@end

@implementation MTWeatherCell

- (void) setWeather:(MTBaseWeather *)currentWeather {
	
	NSString *icon = currentWeather.icon;
	NSString *desc = currentWeather.desc;
	
	if (icon)
		[self.weatherIcon setImageWithURL:[NSURL URLWithString:icon]];
	
	if (desc)
		self.weatherDesc.text = desc;
	
	if (currentWeather.tempC)
		self.temperature.text = [NSString stringWithFormat:@"%@˚C", currentWeather.tempC];
}

@end
